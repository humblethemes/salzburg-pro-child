<?php
add_action( 'wp_enqueue_scripts', 'salzburg_pro_child_enqueue_styles' );
function salzburg_pro_child_enqueue_styles() {
    $style_src = WP_DEBUG ? 'style.css' : 'style.min.css';
    wp_enqueue_style( 'salzburg-styles', get_template_directory_uri() . '/assets/styles/'. $style_src .'' );
}
